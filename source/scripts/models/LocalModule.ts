export interface LocalModule<SomeDefaultExport extends any = any> {
  default: SomeDefaultExport
}
